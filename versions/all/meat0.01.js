//MeatJS v0.01
//closure (Self-Invoking Anonymous Function (SIAF)):
(function() {
    //define private meat variable:
    var meat = {};
    //create meat functions:
    meat.functions = function(element) {
        var funcObj = {
            el : element,
            hide : function() {
                this.el.style.display = "none";
                return this;
            },
            show : function() {
                this.el.style.display = "block";
                return this;
            },
            background : function(hex) {
                this.el.style.backgroundColor = hex;
                return this;
            },
            foreground : function(hex) {
                this.el.style.color = hex;
                return this;
            },
            setSize : function(width, height) {
                if(width) {
                    this.el.style.width = width + "px";
                }
                if(height) {
                    this.el.style.height = height + "px";
                }
                return this;
            },
            setFontSize : function(size) {
                this.el.style.fontSize = size + "px";
                return this;
            },
            setText : function(content) {
                this.el.innerText = content;
                return this;
            },
            setHTML : function(html) {
                this.el.innerHTML = html;
                return this;
            },
            align : function(side) {
                this.el.style.textAlign = side;
                return this;
            },
            fontFamily : function(family) {
                this.el.style.fontFamily = family;
            }
        };
        return funcObj;
    };
    //create dom query function
    meat.dom = function(style) {
        return [meat.functions(document.querySelector(style)), document.querySelector(style)];
    };
    window.meat = meat;
})();