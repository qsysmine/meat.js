//MeatJS v0.02 Prerelease 1
/*|************************
*|||||||||||||||||||||||||*
*|||||||COPYRIGHT|2013||||*
*|||||||THE|Q|PROJECT|||||*
*|||||||||||||||||||||||||*
***************************/
//closure (Self-Invoking Anonymous Function (SIAF)):
(function() {
    //define private meat variable:
    var meat = {};
    //create meat functions:
    meat.functions = function(element) {
        var funcObj = {
            el : element,
            //hide element
            hide : function(callback) {
                this.el.style.display = "none";
                if(callback) {
                        callback();
                    }
                return this;
                
            },
            //show element
            show : function(callback) {
                this.el.style.display = "block";
                if(callback) {
                        callback();
                    }
                return this;
            },
            //set element background
            background : function(hex, callback) {
                this.el.style.backgroundColor = hex;
                if(callback) {
                        callback();
                    }
                return this;
            },
            //set element foreground
            foreground : function(hex, callback) {
                this.el.style.color = hex;
                if(callback) {
                        callback();
                    }
                return this;
            },
            //set element size
            size : function(width, height, callback) {
                if(width) {
                    //if user put in the width
                    this.el.style.width = width + "px";
                }
                if(height) {
                    //if user put in the height
                    this.el.style.height = height + "px";
                }
                if(callback) {
                        callback();
                    }
                return this;
            },
            textSize : function(size, callback) {
                //set font size
                this.el.style.fontSize = size + "px";
                if(callback) {
                        callback();
                    }
                return this;
            },
            html : function(html, callback) {
                this.el.innerHTML = html;
                if(callback) {
                        callback();
                    }
                return this;
            },
            align : function(side, callback) {
                this.el.style.textAlign = side;
                if(callback) {
                        callback();
                    }
                return this;
            },
            font : function(family, callback) {
                this.el.style.fontFamily = family;
                if(callback) {
                        callback();
                    }
                return this;
            },
            obj : function() {
                return this.el;
            }
        };
        return funcObj;
    };
    //create dom query function
    var get = function(style) {
        return meat.functions(document.querySelector(style));
    };
    window.meat = meat;
    if(!window._) {
    window._ = get;
    }
    if(!window.get) {
    window.get = get;
    }
    if(!window.$) {
    window.$ = get;
    }
    window.meat._ = get;
    
})();